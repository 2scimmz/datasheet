using System;
using System.IO;
using System.IO.Compression;
using System.Net.Http;
using System.Threading.Tasks;

namespace LauncherUpdater
{
    class Program
    {
        private static async Task<bool> DownloadAndUnzip(string requestUri, string directoryToUnzip)
        {
            using var response = await new HttpClient().GetAsync(requestUri);
            if (!response.IsSuccessStatusCode) return false;

            using var streamReader = await response.Content.ReadAsStreamAsync();
            using var zip = new ZipArchive(streamReader);
            zip.ExtractToDirectory(directoryToUnzip);
            return true;
        }

        static async Task Main(string[] args)
        {
            var omniteraLauncher = "OmniTERALauncher.exe";
            var parentDirectory = Directory.GetParent(Environment.CurrentDirectory).ToString();
            var launcherPath = Path.Combine(parentDirectory, omniteraLauncher);
            var launcherUrl = "https://gitlab.com/mogus15/datasheet/-/raw/main/Launcher/OmniTERALauncher.zip?inline=false";

            Console.WriteLine("Welcome! To update your launcher press Y otherwise press any other key to exit.");

            var input = Console.ReadLine();
            if (input == "Y") 
            { 
                try
                {
                    if (File.Exists(launcherPath))
                    {
                        Console.WriteLine("Detected existing launcher! Removing...");
                        File.Delete(launcherPath);
                    }
                    await DownloadAndUnzip(launcherUrl, parentDirectory);
                    Console.WriteLine("Download Complete! Press any key to exit.");
                    Console.ReadLine();
                }
                catch (Exception ex) 
                {
                    Console.WriteLine("Error: {0}", ex);
                    Console.WriteLine("Press any key to exit.");
                    Console.ReadLine();
                }
            }
            else { Environment.Exit(0); }
        }
    }
}
