module.exports = function NinjaAA(mod) {
    const { command } = mod;

    let enabled = true,
        AA_RATIO = 1.5;

    command.add("ninjaaa", (ratio) => {
        ratio = parseFloat(ratio);

        if (isNaN(ratio)) {
            enabled = !enabled;
            command.message(`Ninja AA script is now ${enabled ? 'en' : 'dis'}abled.`);
        } else {
            AA_RATIO = ratio;
            command.message(`AA_RATIO temporarily set to ${ratio}.`);
        }
    });
    mod.hook('S_ACTION_STAGE', 9, { order: 10000000, filter: { fake: true } }, event => {
        if (!enabled || AA_RATIO <= 1 || mod.game.me.class !== "assassin") return;
        
        if (Math.floor(event.skill.id / 1e4) == 1) {
            event.speed = event.speed * AA_RATIO;
            event.projectileSpeed = event.speed;
            return true;
        }
    });
}