module.exports = function BrokerBankAnywhere(mod) {
	let enabled = false;
	if(!mod.command && mod.require) { mod.command = mod.require['command']; }//Pinki Proxy Compability
	mod.command.add('broker', () => { mod.toClient('S_NPC_MENU_SELECT', 1, { type: 28 }); });
	mod.command.add(['bank','b'], () => { mod.toServer('C_REQUEST_CONTRACT', 1, { type: 26, gameId: 0, button: 1, name: "", data: Buffer.from([1,0,0,0]) }); });		
	mod.command.add('gbank', () => { mod.toServer('C_REQUEST_CONTRACT', 1, { type: 26, gameId: 0, button: 1, name: "", data: Buffer.from([3,0,0,0]) }); });
	mod.command.add('wardrobe', () => { mod.toServer('C_REQUEST_CONTRACT', 1, { type: 26, gameId: 0, button: 1, name: "", data: Buffer.from([12,0,0,0]) }); });
	mod.command.add('merchant', () => { mod.toServer('C_REQUEST_CONTRACT', 1, { type: 9, gameId: 0, button: 1, name: "", data: Buffer.from([70310,0,0,0]) }); });
	mod.command.add('petbank', () => { mod.toServer('C_REQUEST_CONTRACT', 1, { type: 26, gameId: 0, button: 1, name: "", data: Buffer.from([9,0,0,0]) }); });
	mod.command.add('angler', () => { mod.toServer('C_REQUEST_CONTRACT', 1, { type: 20, gameId: 0, button: 1, name: "", data: Buffer.from([16095,0,0,0]) }); });

	mod.hook('C_REQUEST_CONTRACT',1,(e) => {
		if(!enabled) return;
		mod.log('type: ' + e.type);
		mod.log('unk2: ' + e.unk2);
		mod.log('unk3: ' + e.unk3);
		mod.log('unk4: ' + e.unk4);
		mod.log('name: '+ e.name);
		mod.log(Buffer.from(e.data));
		mod.log(e.data);
		mod.log(JSON.stringify(e.data));
		mod.log(e);
		
		var buffer = new ArrayBuffer(e.data);
			mod.log(buffer);
	})
	
	mod.hook('S_REQUEST_CONTRACT',2,(e) => {
		if(!enabled) return;
		mod.log(e);
		var buffer = new ArrayBuffer(e.data);
		mod.log(buffer);
				mod.log(JSON.stringify(e.data));
	})
	/* 
	
	teleporter
	
	[01:08:40.843] [ethical-anywhere] {
  type: 15,
  unk2: 0,
  unk3: 0,
  unk4: 1,
  name: '',
  data: <Buffer 01 00 00 00>
}
	
	friend summon
	
	[19:09:57.268] [ethical-anywhere] type: 57
	[19:09:57.272] [ethical-anywhere] unk2: 0
	[19:09:57.277] [ethical-anywhere] unk3: 0
	[19:09:57.282] [ethical-anywhere] unk4: 2
	[19:09:57.286] [ethical-anywhere] name: Icyy
	[19:09:57.290] [ethical-anywhere] data: 
	
	
	S_NPC_MENU_SELECT.1
	{"type":1} = speciality merchant
	
	*/
}