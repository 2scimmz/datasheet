## README 

* Datasheet from original 100.2 leak with some changes -> https://forum.ragezone.com/f797/tera-level-100-version-1205489/
* Modified tera-toolbox for patch 100.2 -> https://github.com/tera-private-toolbox/tera-toolbox 
* Shinra-toolbox for patch 100.2 -> https://github.com/Foglio1024/shinra-toolbox-release | https://github.com/tera-private-mods/shinra-toolbox
* Client datacenter for patch 100.2 -> (ragezone link)

